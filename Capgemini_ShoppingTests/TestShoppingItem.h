//
//  TestShoppingItem.h
//  Capgemini_ShoppingTests
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingItem.h"

@interface TestItemAplha : NSObject <ShoppingItem>

@end

@interface TestItemBravo : NSObject <ShoppingItem>

@end
