//
//  ShoppingBasketTests.m
//  Capgemini_ShoppingTests
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ShoppingBasket.h"
#import "TestShoppingItem.h"

@interface ShoppingBasketTests : XCTestCase

@property ShoppingBasket *systemUnderTest;

@end

@implementation ShoppingBasketTests

- (void)setUp {
    [super setUp];
    
    self.systemUnderTest = [ShoppingBasket new];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.systemUnderTest = nil;
    [super tearDown];
}

- (void)testIsEmpty {
    
    NSUInteger numberOfItems = [self.systemUnderTest numberOfItems];
    
    XCTAssertEqual(numberOfItems, 0);
}

- (void)testAddNil {
    
    [self.systemUnderTest add:nil];
    NSUInteger numberOfItems = [self.systemUnderTest numberOfItems];
    
    XCTAssertEqual(numberOfItems, 0);
}

- (void)testAddSingleItem {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    NSUInteger numberOfItems = [self.systemUnderTest numberOfItems];
    
    XCTAssertEqual(numberOfItems, 1);
}

- (void)testAddMultipleItems {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    NSUInteger numberOfItems = [self.systemUnderTest numberOfItems];
    
    XCTAssertEqual(numberOfItems, 3);
}

- (void)testOrderTotal {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    NSDecimalNumber *expectOrderTotal = [NSDecimalNumber decimalNumberWithDecimal:[@3 decimalValue]];
    NSDecimalNumber *orderTotal = [self.systemUnderTest orderTotal];
    
    XCTAssertEqualObjects(orderTotal, expectOrderTotal);
}

- (void)testZeroAmount {
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemAplha new]];
    XCTAssertEqual(amount, 0);
}

- (void)testSingleAmount {
    
    [self.systemUnderTest add:[TestItemAplha new]];
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemAplha new]];
    XCTAssertEqual(amount, 1);
}

- (void)testMultipleAmount {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemAplha new]];
    
    XCTAssertEqual(amount, 3);
}

- (void)testMixedZeroAmount {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemBravo new]];
    
    XCTAssertEqual(amount, 0);
}

- (void)testMixedSingleAmount {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemBravo new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemBravo new]];
    
    XCTAssertEqual(amount, 1);
}

- (void)testMixedMultipleAmount {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemBravo new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemBravo new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    NSUInteger amount = [self.systemUnderTest amountOf:[TestItemBravo new]];
    
    XCTAssertEqual(amount, 2);
}

- (void)testPositiveContains {
    
    [self.systemUnderTest add:[TestItemAplha new]];
    
    BOOL contains = [self.systemUnderTest contains:[TestItemAplha new]];
    XCTAssertTrue(contains);
}

- (void)testEmptyBasketContans {
    
    BOOL contains = [self.systemUnderTest contains:[TestItemAplha new]];
    XCTAssertFalse(contains);
}

- (void)testBasketNegativeContans {
    
    [self.systemUnderTest add:[TestItemBravo new]];
    
    BOOL contains = [self.systemUnderTest contains:[TestItemAplha new]];
    XCTAssertFalse(contains);
}

- (void)testRemoveAllItems {
    
    [self.systemUnderTest add: [TestItemAplha new]];
    [self.systemUnderTest add: [TestItemAplha new]];
    
    [self.systemUnderTest removeAllItems];
    
    NSUInteger numberOfItems = [self.systemUnderTest numberOfItems];
    
    XCTAssertEqual(numberOfItems, 0);
}

@end
