//
//  TestShoppingItem.m
//  Capgemini_ShoppingTests
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import "TestShoppingItem.h"

@implementation TestItemAplha

@synthesize name;
@synthesize price;
@synthesize uuid;

- (instancetype)init
{
    self = [super init];
    if (self) {
        name = NSStringFromClass([self class]);
        price = [NSDecimalNumber decimalNumberWithDecimal:[@1 decimalValue]];
        uuid = [NSString stringWithFormat:@"%@-%lu", name, name.hash];
    }
    return self;
}

@end

@implementation TestItemBravo

@synthesize name;
@synthesize price;
@synthesize uuid;

- (instancetype)init
{
    self = [super init];
    if (self) {
        name = NSStringFromClass([self class]);
        price = [NSDecimalNumber decimalNumberWithDecimal:[@0.10 decimalValue]];
        uuid = [NSString stringWithFormat:@"%@-%lu", name, name.hash];
    }
    return self;
}

@end

