//
//  Capgemini_ShoppingTests.m
//  Capgemini_ShoppingTests
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ShoppingBasket.h"
#import "DiscountGenerator.h"
#import "Fruits.h"

@interface Capgemini_ShoppingTests : XCTestCase

@property ShoppingBasket *SUT;

@end

@implementation Capgemini_ShoppingTests

- (void)setUp {
    [super setUp];
    
    [self setSUT:[ShoppingBasket new]];
}

- (void)tearDown {
    
    [self setSUT:nil];
    
    [super tearDown];
}

- (void)testCorrectPriceSingleItems {
    
    [self.SUT add:[Apple new]];
    [self.SUT add:[Orange new]];
    
    NSDecimalNumber *expectedPrice = [NSDecimalNumber decimalNumberWithDecimal:[@0.85 decimalValue]];
    NSDecimalNumber *totalCost = [self.SUT orderTotal];
    
    XCTAssertEqualObjects(totalCost, expectedPrice);
}

- (void)testCorrectPriceMultipleItems {
    
    [self.SUT add:[Apple new]];     // 0.6
    [self.SUT add:[Apple new]];     // FREE
    [self.SUT add:[Orange new]];    // 0.25
    [self.SUT add:[Orange new]];    // 0.25
    [self.SUT add:[Apple new]];     // 0.60
    [self.SUT add:[Orange new]];    // FREE
    
    
    NSDecimalNumber *expectedPrice = [NSDecimalNumber decimalNumberWithDecimal:[@1.7 decimalValue]];
    NSDecimalNumber *totalCost = [self.SUT orderTotal];
    
    XCTAssertEqualObjects(totalCost, expectedPrice);
}

- (void)testAppleDiscounts {
    
    NSArray *items = @[[Apple new], [Apple new], [Apple new]];
    
    NSDecimalNumber *expectedDiscount = [NSDecimalNumber decimalNumberWithDecimal:[@0.6 decimalValue]];
    NSDecimalNumber *discount = [DiscountGenerator discountForItems:items];
    
    XCTAssertEqualObjects(expectedDiscount, discount);
}

- (void)testOrangeDiscounts {
    
    NSArray *items = @[[Orange new], [Orange new], [Orange new], [Orange new]];
    
    NSDecimalNumber *expectedDiscount = [NSDecimalNumber decimalNumberWithDecimal:[@0.25 decimalValue]];
    NSDecimalNumber *discount = [DiscountGenerator discountForItems:items];
    
    XCTAssertEqualObjects(expectedDiscount, discount);
}


@end
