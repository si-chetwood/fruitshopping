//
//  main.m
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
