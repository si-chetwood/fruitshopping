//
//  Fruits.h
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingItem.h"

@interface Apple : NSObject <ShoppingItem>

@end

@interface Orange : NSObject <ShoppingItem>

@end
