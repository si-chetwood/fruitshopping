//
//  Fruits.m
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import "Fruits.h"

@implementation Apple

@synthesize name;
@synthesize price;
@synthesize uuid;

- (instancetype)init
{
    self = [super init];
    if (self) {
        name = NSStringFromClass([self class]);
        price = [NSDecimalNumber decimalNumberWithDecimal:[@0.60 decimalValue]];
        uuid = [NSString stringWithFormat:@"%@+%lu", name, name.hash];
    }
    return self;
}

@end

@implementation Orange

@synthesize name;
@synthesize price;
@synthesize uuid;

- (instancetype)init
{
    self = [super init];
    if (self) {
        name = NSStringFromClass([self class]);
        price = [NSDecimalNumber decimalNumberWithDecimal:[@0.25 decimalValue]];
        uuid = [NSString stringWithFormat:@"%@+%lu", name, name.hash];
    }
    return self;
}

@end
