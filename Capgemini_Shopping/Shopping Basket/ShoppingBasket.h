//
//  ShoppingBasket.h
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingItem.h"

@interface ShoppingBasket : NSObject

- (void)add:(id <ShoppingItem>)item;

- (NSDecimalNumber *)orderTotal;

- (NSUInteger) numberOfItems;

- (NSUInteger) amountOf:(id <ShoppingItem>)item;

- (BOOL)contains:(id <ShoppingItem>)item;

- (void)removeAllItems;

@end
