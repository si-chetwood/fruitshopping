//
//  ShoppingBasket.m
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import "ShoppingBasket.h"
#import "DiscountGenerator.h"

@interface ShoppingBasket ()

@property (strong, nonatomic) NSMutableDictionary <NSString *, NSArray <id <ShoppingItem>>*>* items;

@end

@implementation ShoppingBasket

- (instancetype)init
{
    self = [super init];
    if (self) {
        _items = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)add:(id <ShoppingItem>)item {
 
    if (item == nil) return;
    
    NSMutableArray *currentItems = [[self.items objectForKey:item.uuid] mutableCopy];
    
    if (currentItems == nil) {
        currentItems = [NSMutableArray array];
    }
    
    [currentItems addObject:item];
    [self.items setObject:currentItems forKey:item.uuid];
}

- (NSDecimalNumber *)orderTotal {
    
    NSDecimalNumber *sum = [NSDecimalNumber decimalNumberWithDecimal:[@0.0 decimalValue]];
    
    for (NSString *key in self.items) {
        
        NSArray <id <ShoppingItem> > *shoppingItems = self.items[key];
        
        NSDecimalNumber *totalCostOfItems = [shoppingItems.firstObject.price decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithDecimal:[@(shoppingItems.count) decimalValue]]];
        
        NSDecimalNumber *costAfterDiscount = [totalCostOfItems decimalNumberBySubtracting:[DiscountGenerator discountForItems:shoppingItems]];
        
        sum = [sum decimalNumberByAdding:costAfterDiscount];
    }
    
    return sum;
}

- (NSUInteger)numberOfItems {
    
   NSUInteger total = 0;
    
    for (NSString *key in self.items) {
        total += self.items[key].count;
    }
    
    return total;
}

- (void)removeAllItems {
    [self.items removeAllObjects];
}

- (NSUInteger) amountOf:(id <ShoppingItem>)item {
    
    return [[self.items objectForKey:item.uuid] count];
}

- (BOOL)contains:(id <ShoppingItem>)item {
    return [self amountOf:item] != 0;
}

@end
