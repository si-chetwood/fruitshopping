//
//  DiscountGenerator.h
//  Capgemini_Shopping
//
//  Created by Brian @ TAE on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShoppingItem.h"

@interface DiscountGenerator : NSObject

+ (NSDecimalNumber *)discountForItems:(NSArray <id <ShoppingItem> >*)items;

@end
