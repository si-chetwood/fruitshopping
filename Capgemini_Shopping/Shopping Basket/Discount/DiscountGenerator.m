//
//  DiscountGenerator.m
//  Capgemini_Shopping
//
//  Created by Brian @ TAE on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import "DiscountGenerator.h"
#import "Fruits.h"

@implementation DiscountGenerator

+ (NSDecimalNumber *)discountForItems:(NSArray <id <ShoppingItem> >*)items {
    
    if ([items count] == 0) return [NSDecimalNumber zero];
    
    id <ShoppingItem> firstItem = [items firstObject];
    
    if ([firstItem isKindOfClass:[Apple class]]) {
        return [DiscountGenerator buy:2 getFree:1 forIems:items];
    }
    
    if ([firstItem isKindOfClass:[Orange class]]) {
        return [DiscountGenerator buy:3 getFree:1 forIems:items];
    }
    
    return [NSDecimalNumber zero];
}

+ (NSDecimalNumber *) buy:(NSUInteger)boughtAmount getFree:(NSUInteger)freeAmount forIems:(NSArray <id <ShoppingItem> >*)items {
    
    id <ShoppingItem> item = items.firstObject;
    
    double freeItems = items.count - ceil(((double)items.count / boughtAmount) * (boughtAmount - freeAmount));
    
    NSDecimalNumber *discountPrince = [NSDecimalNumber decimalNumberWithDecimal:[@(freeItems) decimalValue]];
    
    return [discountPrince decimalNumberByMultiplyingBy:item.price];
}

@end
