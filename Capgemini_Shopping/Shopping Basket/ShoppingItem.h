//
//  ShoppingItem.h
//  Capgemini_Shopping
//
//  Created by Simon Chetwood on 03/05/2018.
//  Copyright © 2018 Simon Chetwood. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShoppingItem <NSObject>

@required
@property (strong, readonly) NSString *name;

@property (strong, readonly) NSDecimalNumber *price;

@property (strong, readonly) NSString *uuid;

@end
